const User = require('./user');
const Notes = require('./notes');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const { JWTSecret } = require('./config/index');

const generateAccessToken = (id) => {
  const payload = {
    id
  };
  return jwt.sign(payload, JWTSecret, { expiresIn: '24h' });
};

class AuthController {
  async registration(req, res) {
    try {
      const err = validationResult(req);
      if (!err.isEmpty()) {
        return res.status(400).send({ message: 'Error message' });
      }
      const { username, password } = req.body;
      const candidate = await User.findOne({ username });
      if (candidate) {
        return res.status(400).send({ message: 'Error message' });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const user = new User({
        username,
        password: hashPassword,
        createdDate: new Date()
      });
      await user.save();
      return res.status(200).send({ message: 'Success' });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: 'Error message' });
    }
  }

  async login(req, res) {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({ username });
      if (!user) {
        return res.status(400).send({ message: 'Error message' });
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).send({ message: 'Error message' });
      }
      const token = generateAccessToken(user._id);
      return res.status(200).send({
        message: 'Success',
        jwt_token: token
      });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: 'Error message' });
    }
  }

  async getUser(req, res) {
    try {
      console.log(req.user.id);
      const user = await User.findById(req.user.id);
      const result = {
        user: {
          _id: user.id,
          username: user.username,
          createdDate: user.createdDate
        }
      };
      res.status(200).send(result);
    } catch (e) {
      res.status(400).send({ message: 'Error message' });
      throw e;
    }
  }

  async getNotes(req, res) {
    try {
        const offset = +req.query.offset || 0
        const limit = +req.query.limit || 0

        const allNotes = await Notes.find({ userId: req.user.id })
        const notes = await Notes.find({ userId: req.user.id })
            .skip(offset)
            .limit(limit).select('-__v')

        return res.status(200).send({
            offset: offset,
            limit: limit,
            count: allNotes.length,
            notes: notes
        })
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

async addNote(req, res) {
    try {
        const textInfo = req.body.text
        const user = await User.findOne({ _id: req.user.id })

        if (!textInfo) {
            return res.status(400).send({ message: "Error message" });
        }

        const note = new Notes({
            userId: user.id,
            completed: false,
            text: textInfo,
            createdDate: new Date()
        })
        await note.save()
        console.log('Note created')

        return res.status(200).send({ message: 'Success' })
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

async getNoteById(req, res) {
    try {
        const note = await Notes.findOne({ _id: req.params.id }).select('-__v')
        if (note) {
            console.log('Get note by id success')
            return res.status(200).send({ note: note })
        } else {
            console.log('No note with this id')
            return res.status(400).send({ message: "Error message" })
        }
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

async updateNoteById(req, res) {
    try {
        if (await Notes.findOneAndUpdate({ _id: req.params.id }, { text: req.body.text })) {
            console.log('Note update success')
            return res.status(200).send({ message: 'Success' })
        } else {
            console.log('No note with this id')
            return res.status(400).send({ message: "Error message" })
        }
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

async checkNoteById(req, res) {
    try {
        const note = await Notes.findOne({ _id: req.params.id })
        const completedValue = note.completed
        if (await Notes.findOneAndUpdate({ _id: req.params.id }, { completed: !completedValue })) {
            console.log('check/uncheck note success')
            return res.status(200).send({ message: "Success" })
        } else {
            console.log('No note with this id')
            return res.status(400).send({ message: "Error message" })
        }
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

async deleteNoteById(req, res) {
    try {
        if (await Notes.findOneAndDelete({ _id: req.params.id })) {
            console.log('Note deleted success')
            return res.status(200).send({ message: 'Success' })
        } else {
            console.log('No note with this id')
            return res.status(400).send({ message: "Error message" })
        }
    } catch (error) {
        console.error(error)
        res.status(400).send({ message: "Error message" })
    }
}

}

module.exports = new AuthController();
