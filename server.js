const express = require("express");
const mongoose = require('mongoose')
const authRouter = require('./authRouter')
const Port = 8080;
require('dotenv').config();
const app = express();
const {mongoUri} = require('./config/index')
const morgan = require("morgan");

app.get("/", (req, res) => {
  res.send("Start");
});

app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(express.urlencoded({ extended: false }));

app.use(express.json())
app.use('/api', authRouter)

const start = async() => {
    console.log('try to start')
  try {
    await mongoose.connect(mongoUri)
    app.listen(Port, () => {
      console.log(`Start at http://localhost:${Port}`);
    });
  } catch(e) {
    console.log(e)
  }
};

start()

