require('dotenv').config();

module.exports = {
     mongoUri: process.env.MONGOBD_URI,
     JWTSecret: process.env.JWT_SECRET
}