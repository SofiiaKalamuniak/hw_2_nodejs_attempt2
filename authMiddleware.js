const jwt = require('jsonwebtoken')
const {JWTSecret} = require('./config/index')

module.exports = function (req, res, next) {
    try {
        const token = req.headers.authorization.split(' ')[1]
        if(!token){
            res.status(400).json({message: 'user is not authorized'})
        }
        const decodedData = jwt.verify(token, JWTSecret)
        req.user = decodedData
        next()
    } catch (e) {
        console.log(e)
        res.status(400).json({message: 'user is not authorized'})
    }
}