const Router = require('express')
const router = new Router()
const controller = require('./authController')
const {check} = require('express-validator')
const authMiddleware = require('./authMiddleware')

router.post('/auth/register',[
    check('username','username cannot be empty').notEmpty(),
    check('password',
    'password must be longer than 6 characters and shorter than 50 characters')
    .isLength({min:6, max:50})
], controller.registration)
router.post('/auth/login', controller.login)
router.get('/users/me', authMiddleware, controller.getUser)
router.get('/notes',authMiddleware, controller.getNotes)
router.post('/notes', authMiddleware, controller.addNote)
router.get('/notes/:id', authMiddleware, controller.getNoteById)
router.put('/notes/:id',authMiddleware, controller.updateNoteById)
router.patch('/notes/:id', authMiddleware, controller.checkNoteById)
router.delete('/notes/:id', authMiddleware, controller.deleteNoteById)

module.exports = router